FROM debian:bookworm-backports
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV NSS_UNKNOWN_HOME=/home/user
ARG DEBIAN_FRONTEND=noninteractive

RUN apt update && \
    apt install -y \
      build-essential \
      libnss-unknown \
      libncurses5-dev \
      rsync \
      cpio \
      python3 \
      unzip \
      bc \
      wget \
      git \
      vim-common

RUN apt update && \
    apt install -y \
      binutils-aarch64-linux-gnu \
      bison \
      flex \
      g++-aarch64-linux-gnu \
      gcc-aarch64-linux-gnu \
      libglib2.0-dev-bin \
      liborc-0.4-dev-bin \
      pkg-config \
      python3-setuptools

# GStreamer needs meson version >= 1.1.
RUN apt update && \
    apt install -y \
      ninja-build \
      python3-pip && \
    pip3 install \
      --break-system-packages \
      meson==1.4.0

# GStreamer dependencies to build waylandsink
RUN apt update && \
    apt install -y \
    libwayland-dev

CMD ["/bin/bash"]
