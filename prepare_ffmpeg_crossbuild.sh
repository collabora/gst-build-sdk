#!/bin/bash

if [ -z $SYSROOT ]; then
  echo "Error: \$SYSROOT is not set, set \$SYSROOT with the absolute path to the gst-build-sdk rootfs folder."
  exit 1
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# TODO this is an example repository containing the V4L2 request API
# Use the mainline git repository once the changes have been merged
repository=https://github.com/jernejsk/FFmpeg
options="--branch v4l2-request-hwaccel-4.4 --single-branch"
destination=$SYSROOT/home/user/ffmpeg

if [ ! -d "$destination" ] ; then
  git clone $repository $options $destination
fi
cp $SCRIPT_DIR/build-scripts/cross_build_ffmpeg.sh $SYSROOT/home/user/ffmpeg
