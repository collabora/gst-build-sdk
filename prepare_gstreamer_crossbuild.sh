#!/bin/sh
set -eu

if [ -z $SYSROOT ]; then
  echo "Error: \$SYSROOT is not set, set \$SYSROOT with the absolute path to the gst-build-sdk rootfs folder."
  exit 1
fi

# Ensure that $SYSROOT is an absolute path; otherwise cross-file expansion breaks.
SYSROOT=$(readlink -f $SYSROOT)
if [ ! -d $SYSROOT ]; then
  echo "Error: \$SYSROOT=$SYSROOT does not exist."
  exit 1
fi

GST_PATH=$SYSROOT/home/user/gstreamer

if [ ! -d "$GST_PATH" ] ; then
  echo "GStreamer does not exist at $GST_PATH; cloning..."
  git clone https://gitlab.freedesktop.org/gstreamer/gstreamer.git $GST_PATH
fi

touch $GST_PATH/cross-file.txt
cat <<EOF > $GST_PATH/cross-file.txt
[host_machine]
system = 'linux'
cpu_family = 'aarch64'
cpu = 'aarch64'
endian = 'little'

[properties]
sys_root = '$SYSROOT'
pkg_config_libdir = '$SYSROOT/usr/lib/pkgconfig:$SYSROOT/usr/lib/aarch64-linux-gnu/pkgconfig:$SYSROOT/usr/share/pkgconfig'

[built-in options]
c_args = ['--sysroot=$SYSROOT']
cpp_args = ['--sysroot=$SYSROOT', '-I$SYSROOT/usr/include/c++/12']
c_link_args = ['--sysroot=$SYSROOT']
cpp_link_args = ['--sysroot=$SYSROOT']

[binaries]
c = 'aarch64-linux-gnu-gcc'
cpp = 'aarch64-linux-gnu-g++'
ar = 'aarch64-linux-gnu-ar'
strip = 'aarch64-linux-gnu-strip'
pkgconfig = 'pkg-config'
EOF

touch $SYSROOT/home/user/run-gstreamer.sh
cat <<EOF > $SYSROOT/home/user/run-gstreamer.sh
#!/bin/sh
/home/user/gstreamer/gst-env.py --sysroot $SYSROOT
EOF
chmod +x $SYSROOT/home/user/run-gstreamer.sh
